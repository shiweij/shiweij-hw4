from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, Intf, TCLink
from mininet.topo import Topo
from mininet.util import dumpNodeConnections

def aggNet():

    net = Mininet( topo=None, controller=Controller, build=False, link=TCLink)
    
    c0 = net.addController('c0', controller=RemoteController, ip='127.0.0.1', port=6633)
    
    s1 = net.addSwitch('s1')
    s2 = net.addSwitch('s2')
    s3 = net.addSwitch('s3')

    h1 = net.addHost('h1', ip='10.1.1.1')
    h2 = net.addHost('h2', ip='10.1.2.1')
    h3 = net.addHost('h3', ip='10.2.1.1')
    h4 = net.addHost('h4', ip='10.3.1.1')
    h5 = net.addHost('h5', ip='10.3.2.1')

    net.addLink(s1,h1)
    net.addLink(s1,h2)
    net.addLink(s1,s2)
    net.addLink(s2,h3,delay='1000ms')
    net.addLink(s2,s3)
    net.addLink(s3,h4)
    net.addLink(s3,h5)


    net.start()
    print "Dumping host connections"
    dumpNodeConnections(net.hosts)

    #TODO: Add your code to test reachability of hosts
    print "Testing network connectivity"
    net.pingAll()
    
    #TODO: Add yoour code to start long lived TCP flows 
    h1 = net.__getitem__("h1")
    h2= net.__getitem__("h2")
    h3 = net.__getitem__("h3")
    h4 = net.__getitem__("h4")
    h5 = net.__getitem__("h5")
    
    h5.cmd("python -m SimpleHTTPServer 80 &")
    h4.cmd("python -m SimpleHTTPServer 80 &")
    h3.cmd("dnsmasq --address=/foo.local/10.3.1.1")
    
    # s1 route add -host 10.1.1.1 h1-eth0
    # s1 arp -s 10.1.1.1 00:00:00:00:00:01

    CLI(net)
    net.stop()
 
if __name__ == '__main__':
    setLogLevel( 'info' )
    aggNet()
